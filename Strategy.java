package queues_simulator;

import java.util.List;

public interface Strategy {

	public void addTask(List<Server> servers, Task t);

}
