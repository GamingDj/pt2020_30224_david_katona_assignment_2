package queues_simulator;

import java.util.List;

public class ConcreteStrategyEven implements Strategy {

	@Override
	public void addTask(List<Server> servers, Task t) {

		Server first;
		int firstSize;
		boolean placed = false;

		first = servers.get(0);
		firstSize = first.getWaitingPeriod().intValue();

		for (Server s : servers) {

			if (s.getWaitingPeriod().intValue() < firstSize) {
				s.addTask(t);
				placed = true;
				break;
			}
		}
		if (placed == false)
			servers.get(0).addTask(t);

	}
}
