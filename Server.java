package queues_simulator;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {

	private BlockingDeque<Task> tasks;
	private AtomicInteger waitingPeriod;
	private Thread t;
	private int id;
	private int clientsServed;

	public Server(int id) {
		this.id = id;
		setClientsServed(0);
		tasks = new LinkedBlockingDeque<Task>();
		waitingPeriod = new AtomicInteger(0);
	}

	public void addTask(Task t) {

		tasks.add(t);
		waitingPeriod = new AtomicInteger(waitingPeriod.incrementAndGet());
	}

	@Override
	public void run() {
		while (true) {
			try {
				if (tasks.isEmpty() == true) {
					Thread.sleep(99 + id);
				} else if (tasks.isEmpty() == false) {
					Task t = tasks.take();
					int init = t.getServiceTime();
					for (int i = 1; i <= t.getServiceTime(); i++) {
						t.setServiceTime(t.getServiceTime() - 1);
						if (t.getServiceTime() != 0 && t.isPut() == false) {
							t.setPut();
							tasks.addFirst(t);
						}
						Thread.sleep(99 + id);
					}
					if (init >= 4)
						tasks.addFirst(t);
					if (t.isCounted() == false) {
						t.setCounted();
						clientsServed++;
						waitingPeriod = new AtomicInteger(waitingPeriod.decrementAndGet());
					}

				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public Task[] getTasks() {
		Task[] res = new Task[waitingPeriod.get()];
		res = (Task[]) tasks.toArray();
		return res;
	}

	public void setTasks(BlockingDeque<Task> tasks) {
		this.tasks = tasks;
	}

	public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(AtomicInteger waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void start() {
		if (t == null) {
			t = new Thread(this, "Queue " + Integer.toString(id));
			t.start();
		}
	}

	@Override
	public String toString() {
		String s;
		s = "\nQueue " + id + ": ";
		for (Task t : tasks) {
			s += t.toString() + " ";
		}
		return s;
	}

	public int getClientsServed() {
		return clientsServed;
	}

	public void setClientsServed(int clientsServed) {
		this.clientsServed = clientsServed;
	}

}
