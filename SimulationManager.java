package queues_simulator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.StringTokenizer;

public class SimulationManager implements Runnable {

	public int nrClient = 0;
	public int nrQ = 0;
	public int tSim = 0;
	public int tArrMin = 0;
	public int tArrMax = 0;
	public int tServMin = 0;
	public int tServMax = 0;
	private float totalWait = 0;
	private String arr;
	private static String serv;
	private StringTokenizer comma;
	private Random rand = new Random();
	private List<Task> generatedTasks;
	private Scheduler scheduler;
	String[] args;

	public SimulationManager(String[] args) {

		this.args = args;
		readFile();
		scheduler = new Scheduler(nrQ);
		generateRandomTasks();
	}

	public int getNrClient() {
		return nrClient;
	}

	public void readFile() {

		try {
			File inFile = new File(args[0]);
			Scanner inScanner = new Scanner(inFile);
			nrClient = inScanner.nextInt();
			nrQ = inScanner.nextInt();
			tSim = inScanner.nextInt();
			inScanner.nextLine();
			arr = inScanner.nextLine();
			comma = new StringTokenizer(arr, ",");
			tArrMin = Integer.parseInt(comma.nextToken());
			tArrMax = Integer.parseInt(comma.nextToken());
			serv = inScanner.nextLine();
			comma = new StringTokenizer(serv, ",");
			tServMin = Integer.parseInt(comma.nextToken());
			tServMax = Integer.parseInt(comma.nextToken());
			inScanner.close();

		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}

	private void generateRandomTasks() {
		generatedTasks = new ArrayList<Task>();
		for (int i = 1; i <= nrClient; i++) {
			int servTime = rand.nextInt(tServMax + 1 - tServMin) + tServMin;
			totalWait += servTime;
			Task t = new Task(i, rand.nextInt(tArrMax + 1 - tArrMin) + tArrMin, servTime);
			generatedTasks.add(t);
		}
		totalWait /= nrClient;
		Collections.sort(generatedTasks);
		for (Task t : generatedTasks)
			System.out.println(t);
	}

	@Override
	public void run() {
		File outFile = new File(args[1]);
		FileWriter fw = null;
		try {
			fw = new FileWriter(outFile);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int currentTime = -1;
		int totalServed = 0;
		while (currentTime < tSim) {
			totalServed = 0;
			currentTime++;
			try {
				fw.write("Time " + currentTime + "\nWaiting clients: ");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			for (Task t : generatedTasks) {
				if (t.getArrivalTime() == currentTime) {
					scheduler.dispatchTask(t);
				} else if (t.getArrivalTime() > currentTime) {
					try {
						fw.write(t.toString());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			for (Server s : scheduler.getServers()) {
				totalServed += s.getClientsServed();
				try {
					fw.write(s.toString());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (totalServed == nrClient) {
				try {
					fw.write("\n\nAverage waiting time: " + totalWait);
					fw.flush();
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.exit(0);
			}
			try {
				fw.write("\n\n");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {

		SimulationManager gen = new SimulationManager(args);
		Thread t = new Thread(gen);
		t.start();

	}

}
