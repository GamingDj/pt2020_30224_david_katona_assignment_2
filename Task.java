package queues_simulator;

public class Task implements Comparable<Task> {

	private int id;
	private int arrivalTime;
	private int serviceTime;
	private int waitTime;
	private boolean put;
	private boolean isCounted;

	public Task(int id, int arrivalTime, int serviceTime) {
		super();
		this.id = id;
		put = false;
		isCounted = false;
		waitTime = 0;
		this.arrivalTime = arrivalTime;
		this.serviceTime = serviceTime;
	}

	public boolean isCounted() {
		return isCounted;
	}

	public void setCounted() {
		this.isCounted = true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public boolean isPut() {
		return put;
	}

	public void setPut() {
		put = true;
	}

	public int getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}

	@Override
	public String toString() {
		return "(" + id + "," + arrivalTime + "," + serviceTime + ") ";
	}

	@Override
	public int compareTo(Task a) {

		if (this.getArrivalTime() < a.getArrivalTime())
			return -1;
		else if (this.getArrivalTime() == a.getArrivalTime())
			return 0;
		else
			return 1;
	}

	public int getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}

}
