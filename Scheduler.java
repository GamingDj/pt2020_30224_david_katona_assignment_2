package queues_simulator;

import java.util.ArrayList;
import java.util.List;

public class Scheduler {

	private List<Server> servers;
	private Strategy strategy;

	public Scheduler(int noServers) {
		servers = new ArrayList<Server>();
		strategy = new ConcreteStrategyEven();
		for (int i = 0; i < noServers; i++) {
			Server s = new Server(i + 1);
			servers.add(s);
			s.start();
		}
	}

	public void dispatchTask(Task t) {

		strategy.addTask(this.servers, t);
	}

	public List<Server> getServers() {
		return servers;
	}
}
